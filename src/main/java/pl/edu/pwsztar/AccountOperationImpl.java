package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountOperationImpl implements AccountOperation {
    private final Map<Integer, Account> accounts = new HashMap<>();

    @Override
    public Account getAccountByNumber(int number) {
        return accounts.get(number);
    }

    @Override
    public Account save(Account account) {
        return accounts.put(account.getAccountNumber(), account);
    }

    @Override
    public void delete(int number) {
        accounts.remove(number);
    }

    @Override
    public List<Account> getAllAccounts() {
        return new ArrayList<>(accounts.values());
    }
}
