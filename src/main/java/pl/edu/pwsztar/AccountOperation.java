package pl.edu.pwsztar;

import java.util.List;

public interface AccountOperation {
    Account getAccountByNumber (int number);
    Account save(Account account);
    void delete(int number);
    List<Account> getAllAccounts();
}
