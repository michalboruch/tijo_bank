package pl.edu.pwsztar;

import java.util.Optional;

class Bank implements BankOperation {
    private final AccountOperation accountOperation;

    private static int accountNumber = 0;
    private static final int EMPTY_BALANCE = 0;

    public Bank() {
        this.accountOperation = new AccountOperationImpl();
    }

    public int createAccount() {
        ++accountNumber;
        accountOperation.save(new Account(accountNumber, EMPTY_BALANCE));

        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        final int deletedAccountBalance = accountBalance(accountNumber);

        if(deletedAccountBalance != ACCOUNT_NOT_EXISTS){
            accountOperation.delete(accountNumber);
        }

        return deletedAccountBalance;
    }

    public boolean deposit(int accountNumber, int amount) {
        final Account account = accountOperation.getAccountByNumber(accountNumber);

        if(Optional.ofNullable(account).isPresent()){
            account.deposit(amount);
            accountOperation.save(account);
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = Optional.ofNullable(accountOperation.getAccountByNumber(accountNumber));
        return account.isPresent() && account.get().getAccountBalance() >= amount;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Optional<Account> account = Optional.ofNullable(accountOperation.getAccountByNumber(fromAccount));
        return account.get().getAccountBalance() >= amount;
    }

    public int accountBalance(int accountNumber) {
        final Account account = accountOperation.getAccountByNumber(accountNumber);
        return Optional.ofNullable(account)
                .map(Account::getAccountBalance)
                .orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {
        return accountOperation.getAllAccounts().stream().mapToInt(account -> account.getAccountBalance()).sum();
    }

    public Account getAccount(int accountNumber){
        return accountOperation.getAccountByNumber(accountNumber);
    }
}
