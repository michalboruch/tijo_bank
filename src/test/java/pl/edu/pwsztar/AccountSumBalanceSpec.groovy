package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountSumBalanceSpec extends Specification {

    @Unroll
    def "returns sum of all accounts balance properly" () {
        given: "initial accounts info"
            Bank bank = new Bank()
            int firstAccount = bank.createAccount()
            int secondAccount = bank.createAccount()
            int thirdAccount = bank.createAccount()
        and: "all accounts have balance"
            bank.deposit(firstAccount, 1000)
            bank.deposit(secondAccount, 250)
            bank.deposit(thirdAccount, 300)
        when: "we calculate all accounts balance"
            int sumBalance = bank.sumAccountsBalance()
        then: "expect sumBalance to be all accounts balance"
            sumBalance == 1550
    }

}
