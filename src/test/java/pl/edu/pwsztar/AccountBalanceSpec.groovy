package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification {

    @Unroll
    def "should return balance of an account"() {

        given: "account that exists"
            Bank bank = new Bank()
            int bankAccountNumber = bank.createAccount()
            bank.deposit(bankAccountNumber, balance)
        when: "wants to get balance"
            int accountBalance = bank.accountBalance(bankAccountNumber);
        then: "balance is equals to #balance"
            accountBalance == balance
        where:
            amount | balance
            1      | 1
            20     | 20
            250    | 250
    }

    def "should not get balance of account that does not exists" () {
        given:
            def bank = new Bank()
        when: "want to get account balance"
            int balance = bank.accountBalance(1);
        then: "account does not exists"
            balance == BankOperation.ACCOUNT_NOT_EXISTS;
    }
}
