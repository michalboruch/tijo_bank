package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification{

    @Unroll
    def "should be able to deposit money to account that exists"() {
        given: "account that exists"
            Bank bank = new Bank()
            int bankAccountNumber = bank.createAccount()
        when: "wants to deposit money"
            boolean isAbleToDeposit = bank.deposit(bankAccountNumber, 1000);
        then: "successful deposit"
            isAbleToDeposit
    }

    def "should not be able to deposit money to account that does not exists"() {
        given: "initial data"
            Bank bank = new Bank()
        when: "wants to deposit money"
            boolean isAbleToDeposit = bank.deposit(1, 1000);
        then: "not successful deposit"
            !isAbleToDeposit
    }
}
