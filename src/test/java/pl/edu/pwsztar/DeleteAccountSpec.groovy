package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "Returns balance of the account that has been deleted" (){
        given: "account that exists"
            Bank bank = new Bank()
            int accountNumber = bank.createAccount()
            bank.deposit(accountNumber, balance)
        when: "account has been deleted"
            int deletedAccountBalance = bank.deleteAccount(accountNumber);
        then: "we should get balance of account that has been deleted"
            deletedAccountBalance == deletedBalance
        where:
            balance ||  deletedBalance
            1000    ||  1000
            300     ||  300
            0       ||  0
    }

    def "Can't delete account that does not exists" (){
        given: "initial data"
            Bank bank = new Bank()
        when: "wants to delete account"
            int deletedAccountBalance = bank.deleteAccount(1);
        then: "account delete is not possible"
            deletedAccountBalance == BankOperation.ACCOUNT_NOT_EXISTS;
    }
}
