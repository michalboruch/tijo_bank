package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "Is able to withdraw money" () {
        given: "initial data"
            Bank bank = new Bank()
            int accountNumber = bank.createAccount()
            bank.deposit(accountNumber, 1000)
        when: "we want to withdraw money"
            boolean isAbleToWithdraw = bank.withdraw(accountNumber, 1000)
        then: "Withdraw successful"
            isAbleToWithdraw
    }

    def "Is not able to withdraw money when is not enough money" () {
        given: "initial data"
            Bank bank = new Bank()
            int accountNumber = bank.createAccount()
            bank.deposit(accountNumber, 1000)
        when: "we want to withdraw more money than is available"
            boolean isAbleToWithdraw = bank.withdraw(accountNumber, 1001)
        then: "Withdraw is not successful"
            !isAbleToWithdraw
    }

    def "Is not able to withdraw money from account that does not exists" () {
        given: "initial data"
            Bank bank = new Bank()
        when: "we want to withdraw money from account that not exists"
            boolean isAbleToWithdraw = bank.withdraw(1, 1000)
        then: "is not able to withdraw"
            !isAbleToWithdraw
    }
}
