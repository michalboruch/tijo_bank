package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification{
    @Unroll
    def "Is possible to transfer money" () {
        given: "Initial data"
            Bank bank = new Bank()
            int firstAccount = bank.createAccount()
            int secondAccount = bank.createAccount()
            bank.deposit(firstAccount, 1000)
        when: "we want to transfer money from first account to second"
            boolean isAbleToTransfer = bank.transfer(firstAccount, secondAccount, 1000)
        then: "transfer is possible"
            isAbleToTransfer
    }

    def "Is not possible to transfer more money that are available on account" () {
        given: "Initial data"
            Bank bank = new Bank()
            int firstAccount = bank.createAccount()
            int secondAccount = bank.createAccount()
            bank.deposit(firstAccount, 1000)
        when: "we want to transfer money from first account to second"
            boolean isAbleToTransfer = bank.transfer(firstAccount, secondAccount, 1001)
        then: "transfer is not possible"
            !isAbleToTransfer
    }
}
